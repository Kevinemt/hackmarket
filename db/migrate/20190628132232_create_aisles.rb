class CreateAisles < ActiveRecord::Migration[5.2]
  def change
    create_table :aisles do |t|
      t.string :name
      t.integer :capacity
      t.references :category
    end
  end
end
