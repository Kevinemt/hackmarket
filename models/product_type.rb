class ProductType < ActiveRecord::Base
    belongs_to :category
    has_many :products
    has_many :batches, through: :products
end