class CreateProductDepartureOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :product_departure_orders do |t|
      t.integer :amount
      t.date :date
      t.references :departure_order
      t.references :product
    end
  end
end
