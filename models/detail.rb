class Detail < ActiveRecord::Base
    belongs_to :invoice
    belongs_to :product

    # scope :moresold, -> { Detail.group(:product_id).count.order(product_id: :desc).first(20).product_id }

end