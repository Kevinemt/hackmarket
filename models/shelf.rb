class Shelf < ActiveRecord::Base
    belongs_to :aisle
    has_many :product_shelves
    has_many :products, through: :product_shelves

end