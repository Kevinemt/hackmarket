require 'colorize'
require_relative 'shopping_cart'
require_relative '../lib/app_shopping/app_shopping'
class Product < ActiveRecord::Base
    belongs_to :batch
    belongs_to :product_type
    has_many :details
    has_many :invoices, through: :details
    has_many :product_departure_orders
    has_many :departure_orders, through: :product_departure_orders
    has_many :product_shopping_carts
    has_many :shopping_carts, through: :product_shopping_carts
    has_many :product_shelves
    has_many :shelves, through: :product_shelves

    
    def self.panaderia
        puts '        Panes de calidad'.colorize(:blue)
        puts

        

        puts 
        puts ' 1.    Comprar producto'
        puts ' 2.    Revisar otro pasillo  '
        puts ' "S"    Salir '

        loop do 
            puts ''
            op = Utils.get_keypressed
            case op

            when "1"
               ShoppingCart.select_product
            when "s"
                puts
                puts "                     ¡Bye!            "
                puts
            else
                system("clear")
                puts "     ----Enter a valid option----" 
                puts 
            end
        break if op.downcase == "s" || op.downcase == "1"
        end
    end
    
    def self.limpieza_del_hogar
        puts
        puts '        Tenemos todo lo que necesitas en productos de limpieza del hogar'.colorize(:blue)
   

    end

    def self.charcuteria_queso_lacteo
        puts
        puts '        Todos los quesos que buscas estan aqui'.colorize(:blue)

    end

    def self.vegetales_fruta
        puts
        puts '       Frutas y vegetales frescos'.colorize(:blue)

    end
     
    def self.harina_pasta_enlatado
        puts
        puts '       Harinas, pastas, arroz y enlatados de las marcas mas conocidas'.colorize(:blue)
    end

    def self.cuidado_aseo_personal
        puts
        puts '       Podras encontras todo lo que necesites para tu aseo personal'.colorize(:blue)

    end

    def self.salsa_condimento
        puts
        puts '        Lo mejor que podras encontrar en salsas y condimentos lo tenemos en KYC'.colorize(:blue)
    end

    def self.carne_ave_cerdo
        puts
        puts '        Carnes variadas de primera'.colorize(:blue)
    end

    def self.alimentos_mascotas
        puts
        puts '        Purina, Dogchow, Supercan, Gatsy'.colorize(:blue)

    end

    def self.bebida_chucherias
        puts
        puts '        Bebidas tradicionales'.colorize(:blue)

    end
    

end