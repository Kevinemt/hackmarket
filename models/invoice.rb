class Invoice < ActiveRecord::Base
    belongs_to :user
    has_many :details
    has_many :products, through: :details
    
end