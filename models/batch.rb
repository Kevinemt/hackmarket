class Batch < ActiveRecord::Base
    belongs_to :warehouse
    has_many :products
    has_many :product_types, through: :products
end