class CreateDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :details do |t|
      t.float :price
      t.float :profit_porcentage
      t.integer :amount
      t.references :invoice
      t.references :product
    end
  end
end
