
require 'csv' #La clase que nos ayuda
=begin
Modo | Sentido
----- + -------------------------------------------- ------------
"r" | Solo lectura, comienza al principio del archivo (modo predeterminado).
----- + -------------------------------------------- ------------
"r +" | Lectura-escritura, comienza al principio del archivo.
----- + -------------------------------------------- ------------
"w" | Solo escritura, trunca el archivo existente
     | a cero de longitud o crea un nuevo archivo para escribir.
----- + -------------------------------------------- ------------
"w +" | Lectura-escritura, trunca el archivo existente a longitud cero
     | o crea un nuevo archivo para leer y escribir.
----- + -------------------------------------------- ------------
"a" | Solo escritura, comienza al final del archivo si el archivo existe,
     | de lo contrario crea un nuevo archivo para escribir.
----- + -------------------------------------------- ------------
"a +" | Lectura-escritura, comienza al final del archivo si el archivo existe,
     | de lo contrario crea un nuevo archivo para leer y
     | escritura.
----- + -------------------------------------------- ------------
"b" | Modo de archivo binario (puede aparecer con
     | cualquiera de las letras clave enumeradas anteriormente).
     | Suprime la conversión EOL <-> CRLF en Windows. Y
     | establece la codificación externa a ASCII-8BIT a menos que explícitamente
     | especificado.
----- + -------------------------------------------- ------------
"t" | Modo de archivo de texto (puede aparecer con
     | cualquiera de las letras clave enumeradas anteriormente excepto "b").
=end


NOMBRE_ARCHIVO = "amorsi.csv" # 
headers = ["nombre", "apellido", "mes", "dia"]
datos_persona = [['cele', 'gomez', 'may', '29'],['anderson', 'sanchez', 'august', '19']]


CSV.open(NOMBRE_ARCHIVO, "a+") do |fila| #primer parametro es el path y el segundo el modo
   
     fila << headers

    datos_persona.each do |d|
        fila << d
    end
end


#  https://parzibyte.me/blog/2019/02/09/leer-escribir-archivos-csv-ruby/