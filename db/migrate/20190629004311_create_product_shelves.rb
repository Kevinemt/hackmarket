class CreateProductShelves < ActiveRecord::Migration[5.2]
  def change
    create_table :product_shelves do |t|
      t.integer :amount
      t.references :shelve
      t.references :product
    end
  end
end
