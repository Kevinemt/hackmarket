require 'colorize'
require 'date'
require_relative '../utils'
require_relative '../../models/user'
require_relative '../../models/role'
require_relative '../../models/category'
require_relative '../../db/connection'
require_relative '../../models/product'
require_relative '../../models/shopping_cart'
class AppShopping

    def log
        puts '
        ██╗  ██╗ █████╗  ██████╗██╗  ██╗███╗   ███╗ █████╗ ██████╗ ██╗  ██╗███████╗████████╗
        ██║  ██║██╔══██╗██╔════╝██║ ██╔╝████╗ ████║██╔══██╗██╔══██╗██║ ██╔╝██╔════╝╚══██╔══╝
        ███████║███████║██║     █████╔╝ ██╔████╔██║███████║██████╔╝█████╔╝ █████╗     ██║   
        ██╔══██║██╔══██║██║     ██╔═██╗ ██║╚██╔╝██║██╔══██║██╔══██╗██╔═██╗ ██╔══╝     ██║   
        ██║  ██║██║  ██║╚██████╗██║  ██╗██║ ╚═╝ ██║██║  ██║██║  ██║██║  ██╗███████╗   ██║   
        ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝   ╚═╝ '.colorize(:yellow)
        puts "
                                    ╔═╗╦ ╦╔═╗╔═╗╔═╗╦╔╗╔╔═╗
                                    ╚═╗╠═╣║ ║╠═╝╠═╝║║║║║ ╦
                                    ╚═╝╩ ╩╚═╝╩  ╩  ╩╝╚╝╚═╝
        
        ".colorize(:green)
    end

    def session_menu
        loop do
            puts "                 1. Login"
            puts
            puts "                 2. Sign in"
            puts
            puts "                 \"S\" -> Exit application" 
            puts
            op = Utils.get_keypressed

            case op
            when "1"
                self.login
            when "2"
                self.sign_in
            when "s"
                puts
                puts "                     ¡Bye!            "
                puts
            else
                system("clear")
                puts "     ----Enter a valid option----" 
                puts 
            end
            break if op.downcase == "s" || op.downcase == "1" || op.downcase == "2"
        end
    end

    def login 
        puts "                  *ENTER USERNAME AND PASSWORD*"
        puts
        username = gets.chomp
        puts
        password = gets.chomp
        puts
        if User.find_by(username: username, password: password).present?
            
            loop do
                puts "          hola #{username} Bienvenido al supermercado KYC".colorize(:yellow)
                puts
                puts '                     ¡Elija el pasillo de su preferencia!    '.colorize(:blue)
                puts
                puts '                 1.       "Panaderia" '
                puts 
                puts '                 2.       "Productos de Limpieza del hogar"    '
                puts
                puts '                 3.       "Charcuteria, quesos y lácteos" '
                puts 
                puts '                 4.       "Vegetales y frutas" '
                puts
                puts '                 5.       "Harinas, pastas y enlatados" '
                puts
                puts '                 6.       "Cuidado y aseo personal"    '
                puts
                puts '                 7.       "Salsas y condimetos" '
                puts
                puts '                 8.       "Carnes, aves y cerdos" '
                puts
                puts '                 9.       "Alimentos de mascotas" '
                puts
                puts '                 10.      "Bebidas y chucherias" '
                puts
                puts "                 \"S\"     -> Exit application" 
                puts
                op = Utils.get_keypressed
  
                case op
    
                when "1"
                    Product.panaderia
                when "2"
                    Product.limpieza_del_hogar
                when "3"
                    Product.charcuteria_queso_lacteo
                when "4"
                    Product.vegetales_fruta
                when "5"
                    Product.harina_pasta_enlatado
                when "6"
                    Product.cuidado_aseo_personal
                when "7"
                    Product.salsa_condimento
                when "8"
                    Product.carne_ave_cerdo
                when "9"
                    Product.alimentos_mascotas
                when "10"
                    Product.bebida_chucherias
                when "s"
                    puts
                    puts "                     ¡Bye!            "
                    puts
                else
                    system("clear")
                    puts "     ----Enter a valid option----" 
                    puts 
                end
    
                break if op.downcase == "s" || op.downcase == "1" || op.downcase == "2" || op.downcase == "3" || op.downcase == "4" || op.downcase == "5" || op.downcase == "6" || op.downcase == "7" || op.downcase == "8" || op.downcase == "9" || op.downcase == "10"
            end
        else
            puts 'Usuario no registrado'
        end
    end

    def sign_in 
        
        datos = {username:'',password:'', full_name:'', id_card:'', address:'', phone:'', date: DateTime.now.to_date, role_id:'1' }
        puts "Rellene los campos requeridos"
        puts
        puts "Enter username"
        dato = gets.chomp
        datos[:username] << dato
        puts
        puts "Enter password"
        dato = gets.chomp
        datos[:password] << dato
        puts
        puts "Enter full name"
        dato = gets.chomp
        datos[:full_name] << dato
        puts
        puts "Enter Id card"
        dato = gets.chomp
        datos[:id_card] << dato
        puts
        puts "Enter address"
        dato = gets.chomp
        datos[:address] << dato
        puts
        puts "Enter phone"
        dato = gets.chomp
        datos[:phone] << dato
        puts
        puts "Datos Ingresados: #{datos}"
        
        u =  User.create(datos)
        if  u.errors.messages.present?
            puts
            puts '          ¡Registro no exitoso! verifique su información '.colorize(:red)
            puts
            puts "Datos incorrectos: #{u.errors.messages}"
        else
            puts 'Registro exitoso'.colorize(:yellow)
        end
    end
end