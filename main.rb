require 'active_record'
require_relative 'lib/utils'

require_relative 'models/aisle'
require_relative 'models/batch'
require_relative 'models/category'
require_relative 'models/departure_order'
require_relative 'models/detail'
require_relative 'models/invoice'
require_relative 'models/product_departure_order'
require_relative 'models/product_shelf'
require_relative 'models/product_shopping_cart'
require_relative 'models/product_type'
require_relative 'models/product'
require_relative 'models/role'
require_relative 'models/shelf'
require_relative 'models/shopping_cart'
require_relative 'models/user'
require_relative 'models/warehouse'

class MainMenu

    def self.seeds
        #Definir Roles
        Role.create(name: 'Cliente')
        Role.create(name: 'Operario')
        Role.create(name: 'Administrador')
        a = User.new(username: 'Kevin', password: 'soykevin', full_name: 'Kevin Monasterio', id_card: '432145', address: 'lejos', phone: '12124524534', date: DateTime.now.to_date, role_id: 1)
        a.save
        b = User.new(username: 'Yeison', password: 'soyyeison', full_name: 'Yeison Gutierrez', id_card: '6864322', address: 'casa', phone: '54668942548', date: DateTime.now.to_date, role_id: 1)
        b.save
        c = User.new(username: 'Celenny', password: 'soycelenny', full_name: 'Celenny Gomez', id_card: '4355523', address: 'cerca', phone: '89367125436', date: DateTime.now.to_date, role_id: 1)
        c.save
        d = User.new(username: 'Juca', password: 'Pucas', full_name: 'Juan Carlos', id_card: '3354833', address: 'lejos', phone: '12359658458', date: DateTime.now.to_date, role_id: 2)
        d.save
        e = User.new(username: 'Carlos', password: 'soycarlos', full_name: 'Carlos Torrealba', id_card: '55225555', address: 'lejos', phone: '12366845214',date: DateTime.now.to_date, role_id: 2)
        e.save
        f = User.new(username: 'Ale', password: 'soyale', full_name: 'Alejandro ya', id_card: '65125456', address: 'lejos', phone: '12376857459', date: DateTime.now.to_date, role_id: 2)
        f.save
        User.create(username: 'Admin', password: 'soyadmin01', full_name: 'HackMarket', id_card: '4565665', address: 'hackmarket', phone: '11113258469',date: DateTime.now.to_date, role_id: 3)
        ShoppingCart.create(user_id: 1)
        ShoppingCart.create(user_id: 2)
        ShoppingCart.create(user_id: 3)
        Category.create(name: 'panaderia')
        Category.create(name: 'limpieza_del_hogar')
        Category.create(name: 'charcuteria_queso_lacteo')
        Category.create(name: 'vegetales_fruta')
        Category.create(name: 'harina_pasta_enlatado')
        Category.create(name: 'cuidado_aseo_personal')
        Category.create(name: 'salsa_condimento')
        Category.create(name: 'carne_ave_cerdo')
        Category.create(name: 'alimentos_mascotas')
        Category.create(name: 'bebida_chucherias')
        Aisle.create(name: 'number_1', capacity: 3, category_id: 1)
        Aisle.create(name: 'number_2', capacity: 3, category_id: 2)
        Aisle.create(name: 'number_3', capacity: 3, category_id: 3)
        Aisle.create(name: 'number_4', capacity: 3, category_id: 4)
        Aisle.create(name: 'number_5', capacity: 3, category_id: 5)
        Aisle.create(name: 'number_6', capacity: 3, category_id: 6)
        Aisle.create(name: 'number_7', capacity: 3, category_id: 7)
        Aisle.create(name: 'number_8', capacity: 3, category_id: 8)
        Aisle.create(name: 'number_9', capacity: 3, category_id: 9)
        Aisle.create(name: 'number_10', capacity: 3, category_id: 10)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 1)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 1)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 1)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 2)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 2)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 2)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 3)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 3)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 3)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 4)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 4)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 4)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 5)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 5)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 5)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 6)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 6)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 6)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 7)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 7)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 7)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 8)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 8)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 8)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 9)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 9)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 9)
        Shelf.create(name:'primero',capacity: 50, aisle_id: 10)
        Shelf.create(name:'segundo',capacity: 50, aisle_id: 10)
        Shelf.create(name:'tercero',capacity: 50, aisle_id: 10)
        Warehouse.create(capacity: 10000000)
        Batch.create(warehouse_id: 1)
        Batch.create(warehouse_id: 1)
        Batch.create(warehouse_id: 1)
        Batch.create(warehouse_id: 1)
        ProductType.create(name: 'canilla', description: 'NA', category_id:1)
        ProductType.create(name: 'acemita', description: 'NA', category_id:1)
        ProductType.create(name: 'campesino', description: 'NA', category_id:1)
        ProductType.create(name: 'cachito', description: 'NA', category_id:1)
        ProductType.create(name: 'desinfectante', description: 'NA', category_id:2)
        ProductType.create(name: 'jabon liquido', description: 'NA', category_id: 2)
        ProductType.create(name: 'cloro', description: 'NA', category_id: 2)
        ProductType.create(name: 'jabon en polvo', description: 'NA', category_id:2)
        ProductType.create(name: 'queso duro', description: 'NA', category_id: 3)
        ProductType.create(name: 'Queso Guayanes', description: 'NA', category_id: 3)
        ProductType.create(name: 'yogurt', description: 'NA', category_id: 3)
        ProductType.create(name: 'leche descremada', description: 'NA', category_id:3)
        ProductType.create(name: 'cambur', description: 'NA', category_id: 4)
        ProductType.create(name: 'cebolla', description: 'NA', category_id: 4)
        ProductType.create(name: 'tomate', description: 'NA', category_id: 4)
        ProductType.create(name: 'ajo', description: 'NA', category_id: 4)
        ProductType.create(name: 'harina pan', description: 'NA', category_id: 5)
        ProductType.create(name: 'pasta', description: 'NA', category_id: 5)
        ProductType.create(name: 'arroz', description: 'NA', category_id:5)
        ProductType.create(name: 'mezcla para cachapas', description: 'NA', category_id:5)
        ProductType.create(name: 'Desodorante', description: 'NA', category_id: 6)
        ProductType.create(name: 'Jabon', description: 'NA', category_id:6)
        ProductType.create(name: 'shampoo', description: 'NA', category_id:6)
        ProductType.create(name: 'Acondicionador', description: 'NA', category_id:6)
        ProductType.create(name: 'Onoto', description: 'NA', category_id:7)
        ProductType.create(name: 'Adobo', description: 'NA', category_id:7)
        ProductType.create(name: 'Oregano', description: 'NA', category_id:7)
        ProductType.create(name: 'Salsa Inglesa', description: 'NA', category_id:7)
        ProductType.create(name: 'Pollo', description: 'NA', category_id:8)
        ProductType.create(name: 'carne', description: 'NA', category_id: 8)
        ProductType.create(name: 'cerdo', description: 'NA', category_id: 8)
        ProductType.create(name: 'Atun', description: 'NA', category_id:8)
        ProductType.create(name: 'Perrarina dogchow', description: 'NA', category_id: 9)
        ProductType.create(name: 'gatarina gatsy', description: 'NA', category_id: 9)
        ProductType.create(name: 'Perrarina Supercan', description: 'NA', category_id:9)
        ProductType.create(name: 'Comida para peces', description: 'NA', category_id:9)
        ProductType.create(name: 'Gran Reserva', description: 'NA', category_id: 10)
        ProductType.create(name: 'Cocuy de penca', description: 'NA', category_id: 10)
        ProductType.create(name: 'Anis', description: 'NA', category_id: 10)
        ProductType.create(name: 'Santa Teresa', description: 'NA', category_id: 10)
        Product.create(price: 10.5, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 1, batch_id: 1)
        Product.create(price: 12, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 2, batch_id: 1)
        Product.create(price: 13, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 3, batch_id: 1)
        Product.create(price: 15, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 4, batch_id: 1)
        Product.create(price: 40, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 5, batch_id: 1)
        Product.create(price: 15.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 6, batch_id: 2)
        Product.create(price: 80.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 7, batch_id: 2)
        Product.create(price: 90.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 8, batch_id: 2)
        Product.create(price: 12.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 9, batch_id: 2)
        Product.create(price: 9.5, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 10, batch_id: 3)
        Product.create(price: 2.5, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 11, batch_id: 3)
        Product.create(price: 1, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 12, batch_id: 3)
        Product.create(price: 4.3, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 13, batch_id: 3)
        Product.create(price: 15, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 14, batch_id: 3)
        Product.create(price: 1.2, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 15, batch_id: 3)
        Product.create(price: 0.8, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 16, batch_id: 3)
        Product.create(price: 1, profit_porcentage: 0.3, batch_amount: 50, product_type_id: 17, batch_id: 4)
        Product.create(price: 2.8, profit_porcentage: 0.35, batch_amount: 50, product_type_id: 18, batch_id: 4)  
        Product.create(price: 15, profit_porcentage: 0.4, batch_amount: 40, product_type_id: 19, batch_id: 4) 
        Product.create(price: 10.5, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 20, batch_id: 1)
        Product.create(price: 12, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 21, batch_id: 1)
        Product.create(price: 13, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 22, batch_id: 1)
        Product.create(price: 15, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 23, batch_id: 1)
        Product.create(price: 40, profit_porcentage: 0.3, batch_amount: 20, product_type_id: 24, batch_id: 1)
        Product.create(price: 15.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 25, batch_id: 2)
        Product.create(price: 80.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 26, batch_id: 2)
        Product.create(price: 90.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 27, batch_id: 2)
        Product.create(price: 12.5, profit_porcentage: 0.3, batch_amount: 25, product_type_id: 28, batch_id: 2)
        Product.create(price: 9.5, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 29, batch_id: 3)
        Product.create(price: 2.5, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 30, batch_id: 3)
        Product.create(price: 1, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 31, batch_id: 3)
        Product.create(price: 4.3, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 32, batch_id: 3)
        Product.create(price: 15, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 33, batch_id: 3)
        Product.create(price: 1.2, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 34, batch_id: 3)
        Product.create(price: 0.8, profit_porcentage: 0.3, batch_amount: 40, product_type_id: 35, batch_id: 3)
        Product.create(price: 1, profit_porcentage: 0.3, batch_amount: 50, product_type_id: 36, batch_id: 4)
        Product.create(price: 2.8, profit_porcentage: 0.35, batch_amount: 50, product_type_id: 37, batch_id: 4)  
        Product.create(price: 15, profit_porcentage: 0.4, batch_amount: 40, product_type_id: 38, batch_id: 4) 
        Product.create(price: 2.8, profit_porcentage: 0.35, batch_amount: 50, product_type_id: 39, batch_id: 4)  
        Product.create(price: 15, profit_porcentage: 0.4, batch_amount: 40, product_type_id: 40, batch_id: 4)   
        
    end

    # def get_keypressed
	#     system("stty raw -echo")
    # 	t = STDIN.getc
	#     system("stty -raw echo")
	#     return t
    # end

    # def self.main
    #     key = 0
    #     loop do
    #         puts '
    #         ██╗  ██╗ █████╗  ██████╗██╗  ██╗███╗   ███╗ █████╗ ██████╗ ██╗  ██╗███████╗████████╗
    #         ██║  ██║██╔══██╗██╔════╝██║ ██╔╝████╗ ████║██╔══██╗██╔══██╗██║ ██╔╝██╔════╝╚══██╔══╝
    #         ███████║███████║██║     █████╔╝ ██╔████╔██║███████║██████╔╝█████╔╝ █████╗     ██║   
    #         ██╔══██║██╔══██║██║     ██╔═██╗ ██║╚██╔╝██║██╔══██║██╔══██╗██╔═██╗ ██╔══╝     ██║   
    #         ██║  ██║██║  ██║╚██████╗██║  ██╗██║ ╚═╝ ██║██║  ██║██║  ██║██║  ██╗███████╗   ██║   
    #         ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝   ╚═╝ 
    #                                 Presione la tecla segun su eleccion
    #                                         1.- Shopping
    #                                         2.- Inventory
    #                                         3.- Bussines Intelligence'
    
    #         key = gets.chomp.to_i
    #         raise StopIteration if key == 1 || key == 2 || key == 3
    #         AppShopping.start if key == 1 # Estos metodos de instacias debes crearlos para que los ejecute aqui, crealo en las carpta de lib para que incien cada app
    #         AppInventory.start if key == 2
    #         AppBussinesIntelligence.start if key == 3
    #     end
    # end

end
