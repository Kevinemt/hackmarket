class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.float :price
      t.float :profit_porcentage
      t.integer :batch_amount
      t.references :product_type
      t.references :batch
    end
  end
end