class CreateDepartureOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :departure_orders do |t|
      t.references :user
    end
  end
end
