# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_29_004834) do

  create_table "aisles", force: :cascade do |t|
    t.string "name"
    t.integer "capacity"
    t.integer "category_id"
    t.index ["category_id"], name: "index_aisles_on_category_id"
  end

  create_table "batches", force: :cascade do |t|
    t.integer "warehouse_id"
    t.index ["warehouse_id"], name: "index_batches_on_warehouse_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
  end

  create_table "departure_orders", force: :cascade do |t|
    t.integer "user_id"
    t.index ["user_id"], name: "index_departure_orders_on_user_id"
  end

  create_table "details", force: :cascade do |t|
    t.float "price"
    t.float "profit_porcentage"
    t.integer "amount"
    t.integer "invoice_id"
    t.integer "product_id"
    t.index ["invoice_id"], name: "index_details_on_invoice_id"
    t.index ["product_id"], name: "index_details_on_product_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.date "buy_date"
    t.integer "user_id"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "product_departure_orders", force: :cascade do |t|
    t.integer "amount"
    t.date "date"
    t.integer "departure_order_id"
    t.integer "product_id"
    t.index ["departure_order_id"], name: "index_product_departure_orders_on_departure_order_id"
    t.index ["product_id"], name: "index_product_departure_orders_on_product_id"
  end

  create_table "product_shelves", force: :cascade do |t|
    t.integer "amount"
    t.integer "shelve_id"
    t.integer "product_id"
    t.index ["product_id"], name: "index_product_shelves_on_product_id"
    t.index ["shelve_id"], name: "index_product_shelves_on_shelve_id"
  end

  create_table "product_shopping_carts", force: :cascade do |t|
    t.integer "amount"
    t.integer "shopping_cart_id"
    t.integer "product_id"
    t.index ["product_id"], name: "index_product_shopping_carts_on_product_id"
    t.index ["shopping_cart_id"], name: "index_product_shopping_carts_on_shopping_cart_id"
  end

  create_table "product_types", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "category_id"
    t.index ["category_id"], name: "index_product_types_on_category_id"
  end

  create_table "products", force: :cascade do |t|
    t.float "price"
    t.float "profit_porcentage"
    t.integer "batch_amount"
    t.integer "product_type_id"
    t.integer "batch_id"
    t.index ["batch_id"], name: "index_products_on_batch_id"
    t.index ["product_type_id"], name: "index_products_on_product_type_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
  end

  create_table "shelves", force: :cascade do |t|
    t.string "name"
    t.integer "capacity"
    t.integer "aisle_id"
    t.index ["aisle_id"], name: "index_shelves_on_aisle_id"
  end

  create_table "shopping_carts", force: :cascade do |t|
    t.integer "user_id"
    t.index ["user_id"], name: "index_shopping_carts_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.string "full_name"
    t.string "id_card"
    t.string "address"
    t.string "phone"
    t.date "date"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  create_table "warehouses", force: :cascade do |t|
    t.integer "capacity"
  end

end
