
class User < ActiveRecord::Base
    belongs_to :role
    has_many :invoices
    has_one :shopping_cart
    has_many :departure_orders

    validates :username, :password, :full_name, :id_card,  :address, :phone, :date, :role_id, presence: { message: "Este campo es obligatorio" }
    validates :username, uniqueness: { message: "Este usuario ya existe" }
    validates :password, length: {  in: 6..10, message: "La clave debe de tener minimo 5  y maximo 10 caracteres" }
    # validates :full_name, format: { with: /\A[a-zA-Z]+\z/, message: "Solo ingrese letras" }
    validates :id_card, length: { in: 6..8, message: "minimo 6 y maximo 8" }
    validates :phone, numericality: { is: 11 , only_integer: true, message:"Ingrese 11 numeros"}
end