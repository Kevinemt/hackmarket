require 'sqlite3'
require 'active_record'

module Connection
    def self.configuration
        file = File.join(File.expand_path('..',__FILE__), 'config.yml')
        YAML.safe_load(File.read(file))
    end

    def self.establish
        ActiveRecord::Base.establish_connection(configuration['development'])
    end
end

Connection.establish