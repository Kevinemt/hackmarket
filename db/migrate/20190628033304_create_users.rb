class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :full_name
      t.string :id_card
      t.string :address
      t.string :phone
      t.date :date
      t.references :role
    end
  end
end
