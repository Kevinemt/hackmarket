class Aisle < ActiveRecord::Base
    belongs_to :category
    has_many :shelves
end