class DepartureOrder < ActiveRecord::Base
    belongs_to :user
    has_many :product_departure_orders
    has_many :products, through: :product_departure_orders
    
end