module Utils
    
    # Returns a key when just pressed (without Enter key)
    def self.get_keypressed
	    system("stty raw -echo")
    	t = STDIN.getc
	    system("stty -raw echo")
	    return t
    end
end
