class Category < ActiveRecord::Base
    has_one :aisle
    has_many :product_types

end