class CreateProductShoppingCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :product_shopping_carts do |t|
      t.integer :amount
      t.references :shopping_cart
      t.references :product
    end
  end
end
